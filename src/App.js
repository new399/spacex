import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import './App.css';

import Home from './pages/Home';
import Search from './pages/Search';

function App() {
  return (
    <Router>
      <Routes>
        <Route exact path ="/" element={<Home/>}/>
        <Route exact path="/search/:searchName" element={<Search/>}/>
      </Routes>
    </Router>
  );
}

export default App;
