import {useState} from 'react';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import {Row, Col, Button, Badge} from 'react-bootstrap';

export default function LaunchCard({launchProp}){

	const history = useNavigate();

	//info from api
	const {flight_number, mission_name, details, links, launch_success, upcoming, launch_date_utc} = launchProp;
	const {mission_patch, article_link, video_link} = links;
	const [status, setStatus] = useState('');

	//view or hide button
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	//shows video, opens new tab
	const showVideo = () => {
		if(video_link === null) {
			win.focus();
		} else {

			var win = window.open(video_link, '_blank');
			win.focus();
		}
	};

	const showArticle = () => {
		if(article_link === null) {
			win.focus();
		} else {

			var win = window.open(article_link, '_blank');
			win.focus();
		}
	};

	return(
		<Col xs={11} className="launchCard mt-4 m-3 p-3">
			
			<h3>
				{mission_name}
				<span className="align-top badge">
					{ upcoming ?
						
						<Badge bg="info">upcoming</Badge>

						:
						<>

							{ launch_success ?

								<Badge bg="success">success</Badge>

								:

								<Badge bg="danger">failed</Badge>

							}
						</>
					}
				</span>
			</h3>

			<div show={show}>
				{ show ?

					<>
						<span class="text-muted">{launch_date_utc}</span> | <a onClick={() => showArticle()} class="link">Article</a> | <a onClick={() => showVideo()} class="link">Video</a>
						<Row className="my-4 justify-content-center">
							<Col xs={6} md={3} lg={2}className="p-3">
								<img src={mission_patch} className="mission_patch" alt={mission_name}/>
							</Col>
							<Col xs={11} md={8} lg={10}>
								{details}
							</Col>
						</Row>
						<Button onClick={handleClose} className="px-4">HIDE</Button>
					</>

					:

					<Button onClick={handleShow} className="px-4">VIEW</Button>

				}
			</div>

		</Col>
	)

}