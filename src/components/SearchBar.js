import {useState} from 'react';
import {Navbar, Form, FormControl, Button, Row, Col} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom'

export default function SearchBar(){

	const [searchName, setSearchName] = useState('');

	const history = useNavigate();

	console.log(searchName);


	const searchLaunches = (searchName) => {

		if (searchName == ""){
			history("/");
		} else {
			
			const url = `/search/${searchName}`;
			console.log(url);
			window.open(url, "_self");
		}
	}

	window.addEventListener("keypress", function(event) {
	  if (event.key === "Enter") {
	    event.preventDefault();
	    searchLaunches(searchName);
	  }
	});

	return(
		<Navbar bg="light" expand="xs" className="p-4 fixed-top" >
		    <FormControl
		        type="search"
		        placeholder="Search..."
		        className="searchBar mx-auto"
		        aria-label="Search"
		        value={searchName}
		        onChange={e => setSearchName(e.target.value)}
		    />
		</Navbar>
	)
}