import {useEffect, useState, useRef} from 'react';
import {Row} from 'react-bootstrap'

import LaunchCard from '../components/LaunchCard';
import SearchBar from '../components/SearchBar';

export default function Home(){

	const triggerRef = useRef(null);
	
	const [launches, setLaunches] = useState('');
	const [offset, setOffset] = useState(0);
	 
	useEffect(() => {
		fetch('https://api.spacexdata.com/v3/launches?order=desc')
		.then(res => res.json())
		.then(data => {

			setLaunches(data.map((launch) => {
				return(
					<LaunchCard key={launch.flight_number} launchProp={launch}/>
				)
			}))
		})
	}, [])

/*	useEffect(() => {
		fetch('https://api.spacexdata.com/v3/launches?order=desc&limit=10&offset=${offset}')
		.then(res => res.json())
		.then(data => {

			setLaunches(data.map((launch) => {
				return(
					<LaunchCard key={launch.flight_number} launchProp={launch}/>
				)
			}))
		})
	}, [offset])*/


	return(
		<div>
			<SearchBar/>
			<Row className="mt-5 pt-5 justify-content-center">
				{launches}
			</Row>
			<p className="text-center my-4">End of list.</p>
		</div>
	)
}