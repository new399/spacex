import {useEffect, useState} from 'react';
import {Row} from 'react-bootstrap'
import {useParams} from 'react-router-dom';

import LaunchCard from '../components/LaunchCard';
import SearchBar from '../components/SearchBar';

export default function Home(){

	const [launches, setLaunches] = useState('');

	const {searchName} = useParams();

	console.log(searchName);

	useEffect(() => {
		fetch(`https://api.spacexdata.com/v3/launches/`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				mission_name: searchName
			})
		})
		.then(res => res.json())
		.then(data => {

			setLaunches(data.map(launch => {
				return(
					<LaunchCard key={launch.flight_number} launchProp={launch}/>
				)
			}))
		})
	}, [])

	return(
		<>
			<SearchBar/>
			<Row className="mt-5 pt-5 justify-content-center">
				{launches}
			</Row>
		</>
	)
}